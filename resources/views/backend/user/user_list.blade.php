@extends('backend.layout.master')

@section('title', 'User List')

@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 >User List</h1>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <!-- /.card-header -->
                        <div class="card-body">
                          <div class="col-112">
                            <?php 
                              $i=1;
                            ?>  
                            <table id="example1" class="table table-bordered table-striped">
                              <thead>
                              <tr>
                                <th width="5%">Serial No.</th>
                                <th>User Name</th>
                                <th>Email</th>
                                <th>Member ID</th>
                                <th>Registration Date</th>
                              </tr>
                              </thead>
                              @foreach($userData as $model)
                              <tr>
                                <td>{{$i++}}</td>
                                <td>{{$model->name}}</td>
                                <td>{{$model->email}}</td>
                                <td>{{$model->member_id}}</td>
                                <td>{{$model->created_at}}</td>
                              </tr>
                              @endforeach()
                            </table>
                          </div>  
                        </div>
                        <!-- /.card-body -->
                    </div>

                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
</div>


@endsection