<aside class="main-sidebar sidebar-dark-primary elevation-4">
  <!-- Brand Logo -->
  <a href="{{route('home')}}" class="brand-link">
  <img src="http://library.lo/bookworm.png"
          alt="AdminLTE Logo"
          class="brand-image img-circle elevation-3"
          style="opacity: .8">
    <span class="brand-text font-weight-light">BookWorm</span>
  </a>

  <!-- Sidebar -->
  <div class="sidebar">
    <!-- Sidebar user panel (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
      <div class="image">
        @if(Auth::user()->image)
        <img src="dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
        @endif
      </div>
      <div class="info">
        <a href="#" class="d-block">{{Auth::user()->name}}</a>
      </div>
    </div>

    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
              with font-awesome or any other icon font library -->

          <li class="nav-item">
              <a href="/dashboard" class="nav-link {{Request::is('dashboard') ? 'active' : ''}}">
              <i class="fas fa-columns"></i>
              <p>
                  DashBoard
              </p>
              </a>
          </li>
        @if(Auth::User()->status==1)
        <li class="nav-item has-treeview {{Request::is('category') || Request::is('writer') ? 'menu-open' : ''}}">
          <a href="#" class="nav-link {{Request::is('category') || Request::is('writer') ? 'active' : ''}}">
            <i class="nav-icon fas fa-edit"></i>
            <p>
              Setup
              <i class="fas fa-angle-left right"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="{{url('category')}}" class="nav-link {{Request::is('category') ? 'active' : ''}}">
                <i class="fas fa-list-ol"></i>
                <p>Category</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{route('writer.index')}}" class="nav-link {{Request::is('writer') ? 'active' : ''}}">
                <i class="fas fa-user-edit"></i>
                <p>Writer</p>
              </a>
            </li>

          </ul>
        </li>
        @endif

        @if(Auth::User()->status==1)
        <li class="nav-item ">
          <a href="{{route('user-list')}}" class="nav-link {{Request::is('user-list') ? 'active' : ''}}">
            <i class="fas fa-users"></i>
            <p>
              User List
            </p>
          </a>
        </li>
        @endif

        <li class="nav-item ">
          <a href="{{route('book.index')}}" class="nav-link {{Request::is('book') ? 'active' : ''}}">
            <i class="fas fa-book-open"></i>
            <p>
              Book Info
            </p>
          </a>
        </li>

        @if(Auth::User()->status==1)
        <li class="nav-item ">
          <a href="{{route('student.index')}}" class="nav-link {{Request::is('student') ? 'active' : ''}}">
            <i class="fas fa-user-graduate"></i>
            <p>
              Student Info
            </p>
          </a>
        </li>
        @endif

        @if(Auth::User()->status==1)
        <li class="nav-item ">
          <a href="{{route('book-issue.index')}}" class="nav-link {{Request::is('book-issue') ? 'active' : ''}}">
            <i class="fas fa-book-medical"></i>
            <p>
              Issue Book
            </p>
          </a>
        </li>
        @endif

        @if(Auth::User()->status==1)
        <li class="nav-item ">
          <a href="{{route('due-book-issue-list')}}" class="nav-link {{Request::is('due-book-issue-list') ? 'active' : ''}}">
            <i class="fas fa-book"></i>
            <p>
              Due Book Issue List
            </p>
          </a>
        </li>
        @endif

        <li class="nav-item ">
          <a href="{{route('activity-list')}}" class="nav-link {{Request::is('activity-list') ? 'active' : ''}}">
            <i class="fas fa-chart-line"></i>
            <p>
              Activity List
            </p>
          </a>
        </li>

        @if(Auth::User()->status==1)
        <li class="nav-item ">
          <a href="{{route('fine-list')}}" class="nav-link {{Request::is('fine-list') ? 'active' : ''}}">
            <i class="fas fa-dollar-sign"></i>
            <p>
              Fine List
            </p>
          </a>
        </li>
        @endif
      </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>
  <!-- /.sidebar -->
</aside>
