@extends('backend.layout.master')

@section('title', 'Create Book Issue')

@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h3> Add New Book Issue Data</h3>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <!-- /.card-header -->
                        <form class="form-horizontal" method="POST" action ="{{route('book-issue.store')}}" enctype="multipart/form-data">
                            @csrf
                            <div class="card-body">
                                <!-- class -->
                                <div class="form-group row">
                                    <label for="class" class="col-sm-2 col-form-label">Class<span style="color:red">*</span></label>
                                    <div class="col-sm-8">
                                        <select name="class" id='class_id' class="form-control" required>
                                            <option value="" selected>-Select Class-</option>
                                            <option value="6">Six</option>
                                            <option value="7">Seven</option>
                                            <option value="8">Eight</option>
                                            <option value="9">Nine</option>
                                            <option value="10">Ten</option>
                                        </select>
                                    </div>
                                </div>
                                <!-- studentname -->
                                <div class="form-group row">
                                    <label for="name" class="col-sm-2 col-form-label">Student Name<span style="color:red">*</span></label>
                                    <div class="col-sm-8">
                                    <select name="student_id"  class="form-control" id="student" disabled >
                                        <option>Student List</option>
                                    </select>
                                    </div>
                                </div>
                                <!-- category -->
                                <div class="form-group row">
                                    <label for="category" class="col-sm-2 col-form-label">Category Name<span style="color:red">*</span></label>
                                    <div class="col-sm-8">
                                        <select name="category_id" class="form-control" id="category"  required>
                                            <option value="" selected>-Select Category-</option>
                                            @foreach($category as $model)
                                                <option value="{{$model->id}}">{{$model->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <!-- book -->
                                <div class="form-group row">
                                    <label for="writer" class="col-sm-2 col-form-label">Book List<span style="color:red">*</span></label>
                                    <div class="col-sm-8">
                                    <select name="book_id"  class="form-control" id="book" disabled >
                                        <option>Book List</option>
                                    </select>
                                    </div>
                                </div>
                                <!-- from date -->
                                <div class="form-group row">
                                    <label for="writer" class="col-sm-2 col-form-label">From Date<span style="color:red">*</span></label>
                                    <div class="col-sm-8">
                                    <input type="text" id="orangeDateOfBirthForm" name="from_date" class="form-control validate" required>
                                    
                                    </div>
                                </div>
                                <!-- to date -->
                                <div class="form-group row">
                                    <label for="writer" class="col-sm-2 col-form-label">To Date<span style="color:red">*</span></label>
                                    <div class="col-sm-8">
                                    <input type="text" id="orangeDateOfBirth" name="to_date" class="form-control validate" required>
                                    
                                    </div>
                                </div>

                                <!-- Upload File -->
                                <div class="form-group row">
                                    <label for="file" class="col-sm-2 col-form-label">Image</label>
                                    <div class="col-sm-8">
                                    <input type="file" class="custom-file-upload" name="file">
                                    </div>
                                </div>

                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer float-right">
                                <input type="submit" class="btn btn-success ">
                                <a  class="btn btn-danger" href="{{route('book.create')}}" role="button">Reset</a>
                                <a  class="btn btn-primary" href="{{route('book.index')}}" role="button">Back</a>
                            </div>
                            <!-- /.card-footer -->
                        </form>
                        <!-- /.card-body -->
                    </div>

                </div>
            </div>
        </div>
            <!-- /.container-fluid -->
    </section>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<script>
    // Get Book List 
     
      $(document).ready(function(){
        $('#category').change(function(){
           var category = $('#category').val();//alert(category);
           $('#book').html('');
            $.ajax({
              url:'get-book/{id}',
              type:'GET',
              data:{myID:category},
              dataType: "json",

              success:function(data)
              {
                //alert("category");
                $.each(data, function(key, book)
                 {     
                  // alert(city.city_name)
                  $('#book').prop('disabled', false).css('background','aliceblue').append('<option value="'+book.id+'">'+book.name+'</option>');
                });
              }
          });
        });

        $('#class_id').change(function(){
           var class_id = $('#class_id').val();//alert(class_id);
           $('#student').html('');
            $.ajax({
              url:'get-class-wise-student/{id}',
              type:'GET',
              data:{classID:class_id},
              dataType: "json",

              success:function(data)
              {
                //alert("category");
                $.each(data, function(key, student)
                 {     
                  // alert(city.city_name)
                  $('#student').prop('disabled', false).css('background','aliceblue').append('<option value="'+student.id+'">'+student.name+'('+student.student_id+')'+'</option>');
                });
              }
          });
        });
      });

      // Data Picker Initialization

     
      // Get Student
     
</script>
<script>
        $(document).ready(function(){
            // alert ('Cliecked');
            var from_date=$('input[name="from_date"]'); //our date input has the name "date"
            var to_date=$('input[name="to_date"]');
            var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
            var options={
                format: 'dd/mm/yyyy',
                container: container,
                changeYear: true,
                changeMonth: true,
                todayHighlight: true,
                autoclose: true,
                yearRange: "1930:2100"
            };
            from_date.datepicker(options);
            to_date.datepicker(options);
        });

</script>
@endsection


