@extends('backend.layout.master')

@section('title', 'Due Book Issue List')

@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 >Due Book Issue List</h1>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <!-- /.card-header -->
                        <div class="card-body">
                          <div class="col-112">
                            <?php 
                              $i=1;
                            ?>  
                            <table id="example1" class="table table-bordered table-striped">
                              <thead>
                              <tr>
                                <th width="5%">Serial No.</th>
                                <th>Student Name</th>
                                <th>Student ID</th>
                                <th>Class</th>
                                <th>Book Name</th>
                                <th>Category</th>
                                <th>Issue Date</th>
                                <th>Due Date</th>
                              </tr>
                              </thead>
                              @foreach($data as $model)
                              <tr>
                                <td>{{$i++}}</td>
                                <td>{{$model->name}}</td>
                                <td>{{$model->std_id}}</td>
                                <td>{{$model->class}}</td>
                                <td>{{$model->book_name}}</td>
                                <td>{{$model->category_name}}</td>
                                <td>{{$model->from_date}}</td>
                                <td>{{$model->to_date}}</td>

                                <!-- <form method="POST" action="{{ route('category.destroy', [ 'id'=> $model->id ]) }}">
                                  @csrf
                                  <input type="hidden" name="delete" value="DELETE">
                                  <button type="submit" class="btn btn-danger btn-icon">
                                    <i data-feather="delete"></i>
                                  </button>
                                </form> -->
                                </td>
                              </tr>
                              @endforeach()
                            </table>
                          </div>  
                        </div>
                        <!-- /.card-body -->
                    </div>

                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
</div>


@endsection