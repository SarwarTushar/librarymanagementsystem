@extends('backend.layout.master')

@section('title', 'Edit Book Issue')

@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h3> Add New Book Issue Data</h3>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <!-- /.card-header -->
                        {{ Form::open(['route'=>['book-issue.update',$bookIssueData->id], 'method'=>'PUT' ,'class'=>'form-horizontal','files'=> true]) }}
                            @csrf
                            <div class="card-body">
                                <!-- class -->
                                <div class="form-group row">
                                    <label for="class" class="col-sm-2 col-form-label">Class<span style="color:red">*</span></label>
                                    <div class="col-sm-8">
                                        <select name="class" id='class_id' class="form-control" required>
                                            <option value="" >-Select Class-</option>
                                            <option value="6"{{ $bookIssueData->class=='6' ? 'selected' : ''}}>Six</option>
                                            <option value="7"{{ $bookIssueData->class=='7' ? 'selected' : ''}}>Seven</option>
                                            <option value="7"{{ $bookIssueData->class=='8' ? 'selected' : ''}}>Eight</option>
                                            <option value="9"{{ $bookIssueData->class=='9' ? 'selected' : ''}}>Nine</option>
                                            <option value="10"{{ $bookIssueData->class=='10' ? 'selected' : ''}}>Ten</option>
                                        </select>
                                    </div>
                                </div>
                                <!-- studentname -->
                                <div class="form-group row">
                                    <label for="name" class="col-sm-2 col-form-label">Student Name<span style="color:red">*</span></label>
                                    <div class="col-sm-8">
                                    <select name="student_id"  class="form-control" id="student"  >
                                        <option value="{{$bookIssueData->student_id}}" selected>{{$bookIssueData->student->name}}</option>
                                    </select>
                                    </div>
                                </div>
                                <!-- category -->
                                <div class="form-group row">
                                    <label for="category" class="col-sm-2 col-form-label">Category Name<span style="color:red">*</span></label>
                                    <div class="col-sm-8">
                                        <select name="category_id" class="form-control" id="category"  >
                                            <option value="" >-Select Category-</option>
                                            @foreach($category as $model)
                                                <option value="{{$model->id}}"{{ $bookIssueData->category_id==$model->id ? 'selected' : ''}}>{{$model->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <!-- book -->
                                <div class="form-group row">
                                    <label for="writer" class="col-sm-2 col-form-label">Book List<span style="color:red">*</span></label>
                                    <div class="col-sm-8">
                                    <select name="book_id"  class="form-control" id="book"  >
                                        <option value="{{$bookIssueData->book_id}}" selected>{{$bookIssueData->book->name}}</option>
                                    </select>
                                    </div>
                                </div>
                                <!-- from date -->
                                <div class="form-group row">
                                    <label for="writer" class="col-sm-2 col-form-label">From Date<span style="color:red">*</span></label>
                                    <div class="col-sm-8">
                                        <input type="text" id="orangeDateOfBirthForm" name="from_date" class="form-control validate"value={{ $bookIssueData->to_date ? date('d-m-Y', strtotime($bookIssueData->from_date)) : ''}}  required>
                                    </div>
                                </div>
                                <!-- to date -->
                                <div class="form-group row">
                                    <label for="writer" class="col-sm-2 col-form-label">To Date<span style="color:red">*</span></label>
                                    <div class="col-sm-8">
                                        <input type="text" id="orangeDateOfBirth" name="to_date" class="form-control validate" value={{ $bookIssueData->to_date ? date('d-m-Y', strtotime($bookIssueData->to_date)) : ''}} required>
                                    </div>
                                </div>

                                <!-- Upload File -->
                                <div class="form-group row">
                                    <label for="file" class="col-sm-2 col-form-label">Image</label>
                                    <div class="col-sm-8">
                                    <input type="file" class="custom-file-upload" name="file">
                                    <img src="/uploads/{{$bookIssueData->image}}" width="100" class="img-thumbnail">
                                    <input type="hidden"  name="hidden_file" value="{{$bookIssueData->image}}">
                                    </div>
                                </div>

                                <!-- status -->
                                <div class="form-group row">
                                    <label for="class" class="col-sm-2 col-form-label">Status<span style="color:red">*</span></label>
                                    <div class="col-sm-8">
                                        <select name="status" id='status' class="form-control" required>
                                            <option value="" >-Select Status-</option>
                                            <option value="Issued"{{ $bookIssueData->status=='Issued' ? 'selected' : ''}}>Issued</option>
                                            <option value="Returned"{{ $bookIssueData->status=='Returned' ? 'selected' : ''}}>Returned</option>
                                            <option value="Lost"{{ $bookIssueData->status=='Lost' ? 'selected' : ''}}>Lost</option>
                                            <option value="Damaged"{{ $bookIssueData->status=='damaged' ? 'selected' : ''}}>Damaged</option>
                                        </select>
                                    </div>
                                </div>

                                <!-- Fine -->
                                <div class="form-group row" id="fine">
                                    <label for="fine" class="col-sm-2 col-form-label">Total Fine</label>
                                    <div class="col-sm-8">
                                        <input type="number" id="total_fine" class="form-control" name="fine" placeholder="Total fine" value={{  $bookIssueData->fine }}>
                                    </div>
                                </div>

                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer float-right">
                                <input type="submit" class="btn btn-success ">
                                <a  class="btn btn-danger" href="{{route('book.create')}}" role="button">Reset</a>
                                <a  class="btn btn-primary" href="{{route('book.index')}}" role="button">Back</a>
                            </div>
                            <!-- /.card-footer -->
                        </form>
                        <!-- /.card-body -->
                    </div>

                </div>
            </div>
        </div>
            <!-- /.container-fluid -->
    </section>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<script>
    // Get Book List 
     
      $(document).ready(function(){
        $('#category').change(function(){
           var category = $('#category').val();//alert(category);
           $('#book').html('');
            $.ajax({
              url:'../get-book/{id}',
              type:'GET',
              data:{myID:category},
              dataType: "json",

              success:function(data)
              {
                //alert("category");
                $.each(data, function(key, book)
                 {     
                  // alert(city.city_name)
                  $('#book').prop('disabled', false).css('background','aliceblue').append('<option value="'+book.id+'">'+book.name+'</option>');
                });
              }
          });
        });

        $('#class_id').change(function(){
           var class_id = $('#class_id').val();//alert(class_id);
           $('#student').html('');
            $.ajax({
              url:'../get-class-wise-student/{id}',
              type:'GET',
              data:{classID:class_id},
              dataType: "json",

              success:function(data)
              {
                //alert("category");
                $.each(data, function(key, student)
                 {     
                  // alert(city.city_name)
                  $('#student').prop('disabled', false).css('background','aliceblue').append('<option value="'+student.id+'">'+student.name+'('+student.student_id+')'+'</option>');
                });
              }
          });
        });
      });

      $(function() {
            $('#fine').hide(); 
            $('#status').change(function(){
                if($('#status').val() == 'Lost' || $('#status').val() == 'Damaged' ) {
                    $('#fine').show(); 
                } else {
                    $('#fine').hide(); 
                } 
            });
            if($('#total_fine').val() != '') {
                    $('#fine').show(); 
                }

        });

      // Data Picker Initialization

     
      // Get Student
     
</script>
<script>
        $(document).ready(function(){
            // alert ('Cliecked');
            var from_date=$('input[name="from_date"]'); //our date input has the name "date"
            var to_date=$('input[name="to_date"]');
            var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
            var options={
                format: 'dd/mm/yyyy',
                container: container,
                changeYear: true,
                changeMonth: true,
                todayHighlight: true,
                autoclose: true,
                yearRange: "1930:2100"
            };
            from_date.datepicker(options);
            to_date.datepicker(options);
        });

    </script>

@endsection


