@extends('backend.layout.master')

@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Book List</h1>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header" style="text-align:right;">
                            <a  class="btn btn-primary" href="{{route('book.index')}}" role="button">Back To Book List</a>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                          <div class="col-112"> 
                            <table id="example1" class="table table-bordered table-striped" style="width:100%">
                              <thead>
                              <tr>
                                <th>Book Name</th>
                                <th>Category</th>
                                <th>Writer Name</th>
                                <th>Description</th>
                                <th>Language</th>
                                <th>Price</th>
                                <th>Image</th>
                              </tr>
                              </thead>
                              
                              <tr>
                                <td>{{$book->name}}</td>
                                <td>{{$book->category->name}}</td>
                                <td>{{$book->writer->name}}</td>
                                <td>{{$book->description}}</td>
                                <td>{{$book->language}}</td>
                                <td>{{$book->price}}</td>
                                <td><img src="/uploads/{{$book->image}}" width="100" class="img-thumbnail"></td>
                              </tr>
                              
                            </table>
                          </div>  
                        </div>
                        <!-- /.card-body -->
                    </div>

                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
</div>


@endsection