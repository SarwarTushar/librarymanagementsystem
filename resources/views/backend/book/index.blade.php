@extends('backend.layout.master')

@section('title', 'Book List')

@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Book List</h1>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        @if(Auth::User()->status==1)
                        <div class="card-header" style="text-align:right;">
                            <a  class="btn btn-primary" href="{{route('book.create')}}" role="button">Add New Book</a>
                        </div>
                        @endif
                        <!-- /.card-header -->
                        <div class="card-body">
                          <div class="col-12">
                            <?php
                              $i=1;
                            ?>
                            <table id="example1" class="table table-bordered table-striped">
                              <thead>
                              <tr>
                                <th width="5%">Serial No.</th>
                                <th>Book Name</th>
                                <th>Category</th>
                                <th>Writer Name</th>
                                <th>Language</th>
                                @if(Auth::User()->status==1)
                                <th>Action</th>
                                @endif
                              </tr>
                              </thead>

                              @foreach($book as $model)
                              <tr>
                                <td>{{$i++}}</td>
                                <td>{{$model->name}}</td>
                                <td>{{$model->category->name}}</td>
                                <td>{{$model->writer->name}}</td>
                                <td>{{$model->language}}</td>

                                @if(Auth::User()->status==1)
                                <td>
                                <a  class="btn btn-info" href="{{route('book.show',$model->id)}}" role="button">View</a>
                                <a  class="btn btn-primary" href="{{route('book.edit',$model->id)}}" role="button">Edit</a>
                                {{ Form::open(['route'=>['book.destroy',$model->id], 'method'=>'delete' ,'class'=>'form-horizontal','files'=> true,'style' => 'display:inline',]) }}
                                {{ Form::submit('Delete',['class'=>'btn btn-danger','onclick' => 'return confirm("Are You Sure Want To Delete ?")']) }}
                                {{ Form::close() }}
                                </td>
                                @endif
                              </tr>
                              @endforeach()
                            </table>
                          </div>
                        </div>
                        <!-- /.card-body -->
                    </div>

                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
</div>


@endsection
