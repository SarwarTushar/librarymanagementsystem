@extends('backend.layout.master')

@section('title', 'Add Book')

@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h3> Add New Book</h3>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <!-- /.card-header -->
                        <form class="form-horizontal" method="POST" action ="{{route('book.store')}}" enctype="multipart/form-data">
                            @csrf
                            <div class="card-body">
                                <!-- name -->
                                <div class="form-group row">
                                    <label for="name" class="col-sm-2 col-form-label">Book Name<span style="color:red">*</span></label>
                                    <div class="col-sm-8">
                                    <input type="text" class="form-control" name="name" placeholder="Book Name" required>
                                    </div>
                                </div>
                                <!-- category -->
                                <div class="form-group row">
                                    <label for="category" class="col-sm-2 col-form-label">Category Name<span style="color:red">*</span></label>
                                    <div class="col-sm-8">
                                        <select name="category" class="form-control" required>
                                            <option value="" selected>-Select Category-</option>
                                            @foreach($category as $model)
                                                <option value="{{$model->id}}">{{$model->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <!-- Writer -->
                                <div class="form-group row">
                                    <label for="writer" class="col-sm-2 col-form-label">Writer Name<span style="color:red">*</span></label>
                                    <div class="col-sm-8">
                                        <select name="writer" class="form-control" required>
                                            <option value="" selected>-Select Writer-</option>
                                            @foreach($writer as $model)
                                                <option value="{{$model->id}}">{{$model->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <!-- description -->
                                <div class="form-group row">
                                    <label for="description" class="col-sm-2 col-form-label">Book Description</label>
                                    <div class="col-sm-8">
                                        <textarea class="form-control" name="description" rows="3" placeholder="Enter Description"></textarea>
                                    </div>
                                </div>
                                <!-- Language -->
                                <div class="form-group row">
                                    <label for="language" class="col-sm-2 col-form-label">Language</label>
                                    <div class="col-sm-8">
                                        <select name="language" class="form-control">
                                            <option value="" selected>-Select Language-</option>
                                            <option value="english">English</option>
                                            <option value="bangla">Bangla</option>
                                            <option value="arabic">Arabic</option>
                                            <option value="other">Other</option>
                                        </select>
                                    </div>
                                </div>
                                <!-- price -->
                                <div class="form-group row">
                                    <label for="price" class="col-sm-2 col-form-label">Book Price</label>
                                    <div class="col-sm-8">
                                    <input type="number" class="form-control" name="price" placeholder="Book Price">
                                    </div>
                                </div>
                                <!-- Upload File -->
                                <div class="form-group row">
                                    <label for="file" class="col-sm-2 col-form-label">Upload File/Image</label>
                                    <div class="col-sm-8">
                                    <input type="file" class="custom-file-upload" name="file">
                                    </div>
                                </div>

                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer float-right">
                                <input type="submit" class="btn btn-success ">
                                <a  class="btn btn-danger" href="{{route('book.create')}}" role="button">Reset</a>
                                <a  class="btn btn-primary" href="{{route('book.index')}}" role="button">Back</a>
                            </div>
                            <!-- /.card-footer -->
                        </form>
                        <!-- /.card-body -->
                    </div>

                </div>
            </div>
        </div>
            <!-- /.container-fluid -->
    </section>
</div>


@endsection
