@extends('backend.layout.master')

@section('title', 'Edit Student')

@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h3> Edit Student</h3>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <!-- /.card-header -->
                        {{ Form::open(['route'=>['student.update',$studentData->id], 'method'=>'PUT' ,'class'=>'form-horizontal','files'=> true]) }}
                            @csrf
                            <div class="card-body">
                                <!-- member_id -->
                                <div class="form-group row">
                                    <label for="member_id" class="col-sm-2 col-form-label">Member ID<span style="color:red">*</span></label>
                                    <div class="col-sm-8">
                                    <input type="text" class="form-control" name="member_id" placeholder="Enter Member Id" readonly value="{{$studentData->member_id}}">
                                    </div>
                                </div>
                                <!-- name -->
                                <div class="form-group row">
                                    <label for="name" class="col-sm-2 col-form-label">Student Name<span style="color:red">*</span></label>
                                    <div class="col-sm-8">
                                    <input type="text" class="form-control" name="name" placeholder="Enter Student Name" value="{{$studentData->name}}" required>
                                    </div>
                                </div>
                                <!-- student_id -->
                                <div class="form-group row">
                                    <label for="student_id" class="col-sm-2 col-form-label">Student ID<span style="color:red">*</span></label>
                                    <div class="col-sm-8">
                                    <input type="text" class="form-control" name="student_id" placeholder="Enter Student Id" value="{{$studentData->student_id}}" required>
                                    </div>
                                </div>
                                <!-- class -->
                                <div class="form-group row">
                                    <label for="class" class="col-sm-2 col-form-label">Class<span style="color:red">*</span></label>
                                    <div class="col-sm-8">
                                        <select name="class" class="form-control" required>
                                            <option value="" selected>-Select Class-</option>
                                            <option value="6" {{ $studentData->class=='6' ? 'selected' : ''}}>Six</option>
                                            <option value="7" {{ $studentData->class=='7' ? 'selected' : ''}}>Seven</option>
                                            <option value="8" {{ $studentData->class=='8' ? 'selected' : ''}}>Eight</option>
                                            <option value="9" {{ $studentData->class=='9' ? 'selected' : ''}}>Nine</option>
                                            <option value="10" {{ $studentData->class=='10' ? 'selected' : ''}}>Ten</option>
                                        </select>
                                    </div>
                                </div>
                                <!-- Gender -->
                                <div class="form-group row">
                                    <label for="gender" class="col-sm-2 col-form-label">Gender<span style="color:red">*</span></label>
                                    <div class="col-sm-8">
                                        <select name="gender" class="form-control" required>
                                            <option value="" selected>-Select Gender-</option>
                                            <option value="male" {{ $studentData->gender=='male' ? 'selected' : ''}}>Male</option>
                                            <option value="female" {{ $studentData->gender=='female' ? 'selected' : ''}}>Female</option>
                                            <option value="other" {{ $studentData->gender=='other' ? 'selected' : ''}}>Other</option>
                                        </select>
                                    </div>
                                </div>
                                <!-- Upload File -->
                                <div class="form-group row">
                                    <label for="file" class="col-sm-2 col-form-label">Image</label>
                                    <div class="col-sm-8">
                                    <input type="file" class="custom-file-upload" name="file">
                                    <img src="/uploads/{{$studentData->image}}" width="100" class="img-thumbnail">
                                    <input type="hidden"  name="hidden_file" value="{{$studentData->image}}">
                                    </div>
                                </div>

                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer float-right">
                            {{ Form::submit('Submit',['class'=>'btn btn-success']) }}
                                <a  class="btn btn-danger" href="{{route('student.create')}}" role="button">Reset</a>
                                <a  class="btn btn-primary" href="{{route('student.index')}}" role="button">Back</a>
                            </div>
                            <!-- /.card-footer -->
                        {{ Form::close() }}
                        <!-- /.card-body -->
                    </div>

                </div>
            </div>
        </div>
            <!-- /.container-fluid -->
    </section>
</div>


@endsection