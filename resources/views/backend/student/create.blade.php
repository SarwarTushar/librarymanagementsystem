@extends('backend.layout.master')

@section('title', 'Add Student')

@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h3> Add New Student</h3>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <!-- /.card-header -->
                        <form class="form-horizontal" method="POST" action ="{{route('student.store')}}" enctype="multipart/form-data">
                            @csrf
                            <div class="card-body">
                                <!-- name -->
                                <div class="form-group row">
                                    <label for="name" class="col-sm-2 col-form-label">Student Name<span style="color:red">*</span></label>
                                    <div class="col-sm-8">
                                    <input type="text" class="form-control" name="name" placeholder="Enter Student Name" required>
                                    </div>
                                </div>
                                <!-- student_id -->
                                <div class="form-group row">
                                    <label for="student_id" class="col-sm-2 col-form-label">Student ID<span style="color:red">*</span></label>
                                    <div class="col-sm-8">
                                    <input type="text" class="form-control" name="student_id" placeholder="Enter Student Id" required>
                                    </div>
                                </div>
                                <!-- class -->
                                <div class="form-group row">
                                    <label for="class" class="col-sm-2 col-form-label">Class<span style="color:red">*</span></label>
                                    <div class="col-sm-8">
                                        <select name="class" class="form-control" required>
                                            <option value="" selected>-Select Class-</option>
                                            <option value="6">Six</option>
                                            <option value="7">Seven</option>
                                            <option value="8">Eight</option>
                                            <option value="9">Nine</option>
                                            <option value="10">Ten</option>
                                        </select>
                                    </div>
                                </div>
                                <!-- Gender -->
                                <div class="form-group row">
                                    <label for="gender" class="col-sm-2 col-form-label">Gender<span style="color:red">*</span></label>
                                    <div class="col-sm-8">
                                        <select name="gender" class="form-control" required>
                                            <option value="" selected>-Select Gender-</option>
                                            <option value="male">Male</option>
                                            <option value="female">Female</option>
                                            <option value="other">Other</option>
                                        </select>
                                    </div>
                                </div>
                                <!-- Upload File -->
                                <div class="form-group row">
                                    <label for="file" class="col-sm-2 col-form-label">Image</label>
                                    <div class="col-sm-8">
                                    <input type="file" class="custom-file-upload" name="file">
                                    </div>
                                </div>

                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer float-right">
                                <input type="submit" class="btn btn-success ">
                                <a  class="btn btn-danger" href="{{route('student.create')}}" role="button">Reset</a>
                                <a  class="btn btn-primary" href="{{route('student.index')}}" role="button">Back</a>
                            </div>
                            <!-- /.card-footer -->
                        </form>
                        <!-- /.card-body -->
                    </div>

                </div>
            </div>
        </div>
            <!-- /.container-fluid -->
    </section>
</div>


@endsection