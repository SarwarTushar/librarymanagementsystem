@extends('backend.layout.master')

@section('title', 'Student List')

@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Student List</h1>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header" style="text-align:right;">
                            <a  class="btn btn-primary" href="{{route('student.create')}}" role="button">Add New Student</a>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                          <div class="col-112">
                            <?php 
                              $i=1;
                            ?>  
                            <table id="example1" class="table table-bordered table-striped">
                              <thead>
                              <tr>
                                <th width="5%">Serial No.</th>
                                <th>Member ID</th>
                                <th>Student Name</th>
                                <th>Student ID</th>
                                <th>Class</th>
                                <th>Gender</th>
                                <th>image</th>
                                <th>Action</th>
                              </tr>
                              </thead>
                              @foreach($studentData as $model)
                              <tr>
                                <td>{{$i++}}</td>
                                <td>{{$model->member_id}}</td>
                                <td>{{$model->name}}</td>
                                <td>{{$model->student_id}}</td>
                                <td>{{$model->class}}</td>
                                <td>{{$model->gender}}</td>
                                <td><img src="/uploads/{{$model->image}}" width="100" class="img-thumbnail"></td>

                                <td>
                                <a  class="btn btn-primary" href="{{route('student.edit',$model->id)}}" role="button">Edit</a>
                                {{ Form::open(['route'=>['student.destroy',$model->id], 'method'=>'delete' ,'class'=>'form-horizontal','files'=> true,'style' => 'display:inline',]) }}
                                {{ Form::submit('Delete',['class'=>'btn btn-danger','onclick' => 'return confirm("Are You Sure Want To Delete ?")']) }}
                                {{ Form::close() }}
                                <!-- <form method="POST" action="{{ route('category.destroy', [ 'id'=> $model->id ]) }}">
                                  @csrf
                                  <input type="hidden" name="delete" value="DELETE">
                                  <button type="submit" class="btn btn-danger btn-icon">
                                    <i data-feather="delete"></i>
                                  </button>
                                </form> -->
                                </td>
                              </tr>
                              @endforeach()
                            </table>
                          </div>  
                        </div>
                        <!-- /.card-body -->
                    </div>

                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
</div>


@endsection