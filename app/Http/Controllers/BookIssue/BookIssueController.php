<?php

namespace App\Http\Controllers\BookIssue;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\BookIssue;
use App\Models\Student;
use App\Models\Book;
use App\Models\Setup\Category;
use App\Models\Setup\Writer;
use Auth;
use Illuminate\Support\Facades\DB;

class BookIssueController extends Controller
{

    public function index()
    {
        $bookIssueData=BookIssue::all();
        return view('backend.BookIssue.index',compact("bookIssueData"));
    }

    public function create()
    {
        $student=Student::all();
        $category=Category::get()->all();
        $writer=Writer::get()->all();
        return view('backend.BookIssue.create',compact("category","writer","student"));
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=>'required',
            'category_id'=>'required',
            'student_id'=>'required',
            'book_id'=>'required',
            'class'=>'required',
            'from_date'=>'required',
            'to_date'=>'required',
        ]);
        //dd($request);
        $book_issue=new BookIssue();
        $book_issue->student_id=$request->student_id;
        $book_issue->category_id=$request->category_id;
        $book_issue->book_id=$request->book_id;
        $book_issue->class=$request->class;
        $book_issue->from_date=date("Y-m-d",strtotime($request->from_date));
        $book_issue->to_date=date("Y-m-d",strtotime($request->to_date));
        $book_issue->created_by=Auth::id();
        $book_issue->status="Issued";


        //file Upload
        if ($request->hasFile('file')){
            $file=$request->file('file');
            $extension=$file->getClientOriginalExtension();
            $fileName = time().'.'. $extension;
            $file->move('uploads',$fileName);
            $book_issue->image=$fileName;

        }
        else
        {
            $book_issue->image='';
        }

        $book_issue->save();
        return redirect(route('book-issue.index'))->with('success','Book Issue Has been Created Successfully');
    }

    // public function show($id)
    // {
    //     $book=Book::findorFail($id);
    //     return view('backend.book.show',compact("book"));
    // }

    public function edit($id)
    {
        $bookIssueData=BookIssue::findorFail($id);
        $student=Student::all();
        $category=Category::get()->all();
        $writer=Writer::get()->all();

        return view('backend.BookIssue.edit',compact("category","writer","student","bookIssueData"));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name'=>'required',
            'category_id'=>'required',
            'student_id'=>'required',
            'book_id'=>'required',
            'class'=>'required',
            'from_date'=>'required',
            'to_date'=>'required',
        ]);
        //dd($request);
        $file_name=$request->hidden_file;

        if ($request->hasFile('file')){
            $file=$request->file('file');
            $extension=$file->getClientOriginalExtension();
            $fileName = time().'.'. $extension;
            $file->move('uploads',$fileName);
            $image=$fileName;

        }
        else
        {
            $image=$file_name;
        }

        $edited_data=array(
            'student_id'=>$request->student_id,
            'category_id'=>$request->category_id,
            'book_id'=>$request->book_id,
            'class'=>$request->class,
            'from_date'=>date("Y-m-d",strtotime($request->from_date)),
            'to_date'=>date("Y-m-d",strtotime($request->to_date)),
            'status'=>$request->status,
            'fine'=>$request->fine,
            'image'=>$image,
            'updated_by'=>Auth::id()

        );

        BookIssue::whereId($id)->update($edited_data);
        return redirect(route('book-issue.index'))->with('success','Book Issue Has been Updated Successfully');
    }

    public function destroy($id)
    {
        $bookData=Book::findorFail($id);
        $bookData->delete();
        return redirect()->route('book.index')->with('success','Book Issue Has been Deleted Successfully');

    }

    //categoryWiseBook
    public function getCategoryWiseBook()
    {
        $id = $_GET['myID'];
        $res = DB::table('categories as a')
        ->select('b.*')
        ->leftjoin('books as b','b.category_id','=','a.id')
        ->where('b.category_id', $id)
        ->get();

        return response()->json($res);

    }

    //classWiseStudent
    public function getClassWiseStudent()
    {
        $id = $_GET['classID'];
        $res = DB::table('students')
        ->select('*')
        ->where('class', $id)
        ->get();

        return response()->json($res);

    }

    public function dueBookIssuelist()
    {
        $data= DB::table('book_issues as a')
            ->select('a.*','b.name','b.student_id as std_id','c.name as book_name','d.name as category_name')
            ->leftjoin('students as b','a.student_id','=','b.id')
            ->leftjoin('books as c','a.book_id','=','c.id')
            ->leftjoin('categories as d','a.category_id','=','d.id')
            ->whereRaw("DATE(a.to_date) < CURDATE()")
            ->get();
        //dd($data);
        return view('backend.BookIssue.due-book-issue-list',compact("data"));
    }

    public function activityList()
    {
        $data= DB::table('book_issues as a')
            ->select('a.*','c.name as book_name','d.name as category_name')
            ->leftjoin('students as b','a.student_id','=','b.id')
            ->leftjoin('books as c','a.book_id','=','c.id')
            ->leftjoin('categories as d','a.category_id','=','d.id')
            ->where('b.member_id',Auth::user()->member_id)
            ->get();
        //dd($data);
        return view('backend.BookIssue.activity-list',compact("data"));
    }

    public function fineList()
    {
        $data= DB::table('book_issues as a')
            ->select('a.*','b.*','c.name as book_name','d.name as category_name')
            ->leftjoin('students as b','a.student_id','=','b.id')
            ->leftjoin('books as c','a.book_id','=','c.id')
            ->leftjoin('categories as d','a.category_id','=','d.id')
            ->whereNotNull('a.fine')
            ->get();
        //dd($data);
        return view('backend.fine.fine-list',compact("data"));
    }
}
