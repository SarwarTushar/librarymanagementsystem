<?php

namespace App\Http\Controllers\Book;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\Book;
use App\Models\Setup\Category;
use App\Models\Setup\Writer;
use Auth;

class BookController extends Controller
{

    public function index()
    {
        $book=Book::all();
        $category=Category::all();
        return view('backend.book.index',compact("book","category"));
    }

    public function create()
    {
        $category=Category::get()->all();
        $writer=Writer::get()->all();
        return view('backend.book.create',compact("category","writer"));
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=>'required',
            'category_id'=>'required',
            'writer_id'=>'required',
            'language'=>'required',
        ]);

        $book=new Book();
        $book->name=$request->name;
        $book->category_id=$request->category;
        $book->writer_id=$request->writer;
        $book->price=$request->price;
        $book->description=$request->description;
        $book->language=$request->language;
        $book->created_by=Auth::id();

        //file Upload
        if ($request->hasFile('file')){
            $file=$request->file('file');
            $extension=$file->getClientOriginalExtension();
            $fileName = time().'.'. $extension;
            $file->move('uploads',$fileName);
            $book->image=$fileName;

        }
        else
        {
            $book->image='';
        }

        $book->save();
        return redirect(route('book.index'));
    }

    public function show($id)
    {
        $book=Book::findorFail($id);
        return view('backend.book.show',compact("book"))->with('success','Book Has been Created Successfully');
    }

    public function edit($id)
    {
        $bookData=Book::findorFail($id);
        $category=Category::get()->all();
        $writer=Writer::get()->all();
        return view('backend.book.edit',compact("bookData","category","writer"));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name'=>'required',
            'category_id'=>'required',
            'writer_id'=>'required',
            'language'=>'required',
        ]);

        $file_name=$request->hidden_file;

        if ($request->hasFile('file')){
            $file=$request->file('file');
            $extension=$file->getClientOriginalExtension();
            $fileName = time().'.'. $extension;
            $file->move('uploads',$fileName);
            $image=$fileName;

        }
        else
        {
            $image=$file_name;
        }

        $edited_data=array(
            'name'=>$request->name,
            'category_id'=>$request->category,
            'writer_id'=>$request->writer,
            'price'=>$request->price,
            'description'=>$request->description,
            'language'=>$request->language,
            'image'=>$image,
            'updated_by'=>Auth::id()

        );

        Book::whereId($id)->update($edited_data);
        return redirect(route('book.index'))->with('success','Book Has been Updated Successfully');
    }

    public function destroy($id)
    {
        $bookData=Book::findorFail($id);
        $bookData->delete();
        return redirect()->route('book.index');

    }
}
