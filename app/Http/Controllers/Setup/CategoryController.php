<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\Setup\Category;
use Auth;
use Validator;

class CategoryController extends Controller
{

    public function index()
    {
        $category=Category::all();
        return view('backend.category.index',compact("category"));
    }

    public function create()
    {
        return view('backend.category.create');
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=>'required|unique:categories'
        ]);

        $category=new Category();
        $category->name=$request->name;
        $category->created_by=Auth::id();
        $category->save();
        return redirect(route('category.index'))->with('success','Category Has been Created Successfully');

    }

    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        $categoryData=Category::findorFail($id);
        return view('backend.category.edit',compact("categoryData"));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name'=>'required|unique:categories'
        ]);
        $category=Category::findorFail($id);
        $category->update($request->all());
        return redirect(route('category.index'))->with('success','Category Has been Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $CategoryData=Category::findorFail($id);
        $CategoryData->delete();
        return redirect(route('category.index'))->with('success','Category Has been Deleted Successfully');
    }
}
