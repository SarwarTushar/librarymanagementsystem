<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\Setup\Writer;
use Auth;

class WriterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $writer=Writer::all();
        return view('backend.writer.index',compact("writer"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.writer.create');
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=>'required'
        ]);
        $writer=new Writer();
        $writer->name=$request->name;
        $writer->created_by=Auth::id();
        $writer->save();
        return redirect(route('writer.index'))->with('success','Writer Has been Created Successfully');
    }

    public function edit($id)
    {
        $writerData=Writer::findorFail($id);
        return view('backend.writer.edit',compact("writerData"));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name'=>'required'
        ]);
        $writer=Writer::findorFail($id);
        $writer->update($request->all());
        return redirect(route('writer.index'))->with('success','Writer Has been Updated Successfully');
    }

    public function destroy($id)
    {
        $writerData=Writer::findorFail($id);
        $writerData->delete();
        return redirect()->route('writer.index')->with('success','Writer Has been Deleted Successfully');
    }
}
