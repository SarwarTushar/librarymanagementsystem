<?php

namespace App\Http\Controllers\Student;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Student;
use Auth;

class StudentController extends Controller
{

    public function index()
    {
        $studentData=Student::all();//dd($studentData);
        return view('backend.student.index',compact("studentData"));
    }

    public function create()
    {

        return view('backend.student.create');
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=>'required',
            'student_id'=>'required',
            'class'=>'required',
            'gender'=>'required',
        ]);

        $student=new Student();
        $student->name=$request->name;
        $student->student_id=$request->student_id;
        $student->class=$request->class;
        $student->gender=$request->gender;
        $student->member_id=rand();
        $student->created_by=Auth::id();

        //file Upload
        if ($request->hasFile('file')){
            $file=$request->file('file');
            $extension=$file->getClientOriginalExtension();
            $fileName = time().'.'. $extension;
            $file->move('uploads',$fileName);
            $student->image=$fileName;

        }
        else
        {
            $student->image='';
        }

        $student->save();
        return redirect(route('student.index'))->with('success','Student Has been Created Successfully');
    }

    public function edit($id)
    {
        $studentData=Student::findorFail($id);
        return view('backend.student.edit',compact("studentData"));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name'=>'required',
            'student_id'=>'required',
            'class'=>'required',
            'gender'=>'required',
        ]);

        $file_name=$request->hidden_file;

        if ($request->hasFile('file')){
            $file=$request->file('file');
            $extension=$file->getClientOriginalExtension();
            $fileName = time().'.'. $extension;
            $file->move('uploads',$fileName);
            $image=$fileName;

        }
        else
        {
            $image=$file_name;
        }

        $edited_data=array(
            'name'=>$request->name,
            'student_id'=>$request->student_id,
            'class'=>$request->class,
            'gender'=>$request->gender,
            'member_id'=>$request->member_id,
            'image'=>$image,
            'updated_by'=>Auth::id()

        );

        Student::whereId($id)->update($edited_data);
        return redirect(route('student.index'))->with('success','Student Has been Updated Successfully');
    }

    public function destroy($id)
    {
        $student=Student::findorFail($id);
        $student->delete();
        return redirect()->route('student.index')->with('success','Student Has been Deleted Successfully');

    }
}
