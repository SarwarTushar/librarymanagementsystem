<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\User;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data['total_book']=$this->totalBook();
        $data['total_student']=$this->totalStudent();
        $data['total_user']=$this->totalUser();
        return view('dashboard',$data);
    }

    public function totalBook()
    {
        return DB::table('books as a')
        ->select(DB::raw("COUNT(a.id) as total"))
        ->value('a.total');
    }

    public function totalStudent()
    {
        return DB::table('students as a')
        ->select(DB::raw("COUNT(a.id) as total"))
        ->value('a.total');
    }

    public function totalUser()
    {
        return DB::table('users as a')
        ->select(DB::raw("COUNT(a.id) as total"))
        ->value('a.total');
    }

    public function userList()
    {
        $userData=User::all();
        return view('backend.user.user_list',compact('userData'));
    }

}

