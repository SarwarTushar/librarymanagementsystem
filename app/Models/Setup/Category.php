<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable=['name','created_by','updated_by'];
}
