<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $fillable=['name','student_id','class','gender','image','created_by','updated_by'];
}
