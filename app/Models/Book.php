<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Setup\Category;
use App\Models\Setup\Writer;

class Book extends Model
{
    protected $fillable=['name','category_id','writer_id','price','description','language','image','created_by','updated_by'];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function writer()
    {
        return $this->belongsTo(Writer::class);
    }

}


