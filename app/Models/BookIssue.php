<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Setup\Category;
use App\Models\Setup\Writer;
use App\Models\Student;
use App\Models\Book;

class BookIssue extends Model
{
    protected $fillable=['student_id','category_id','book_id','from_date','to_date','class','image','fine','created_by','updated_by'];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function writer()
    {
        return $this->belongsTo(Writer::class);
    }

    public function student()
    {
        return $this->belongsTo(Student::class);
    }

    public function book()
    {
        return $this->belongsTo(Book::class);
    }
}
