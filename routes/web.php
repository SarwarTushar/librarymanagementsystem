<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//front end home route
Route::get('/', function () {
    return view('frontend.index');
});

Auth::routes();

//backend home route
Route::get('/dashboard', 'HomeController@index')->name('home');

//register Route
Route::get('/new-register', function () {
    return view('auth.register');
});

//user list route
Route::get('/user-list', 'HomeController@userList')->name('user-list');

//Category Setup
Route::resource('/category', 'Setup\CategoryController')->parameter('category','id');

//Writer Setup
Route::resource('/writer', 'Setup\WriterController')->parameter('writer','id');

//Book
Route::resource('/book', 'Book\BookController')->parameter('book','id');

//Student
Route::resource('/student', 'Student\StudentController')->parameter('student','id');

//Book Issue
Route::resource('/book-issue', 'BookIssue\BookIssueController')->parameter('book-issue','id');
Route::get('/book-issue/get-book/{id}', 'BookIssue\BookIssueController@getCategoryWiseBook');
Route::get('/book-issue/get-class-wise-student/{id}', 'BookIssue\BookIssueController@getClassWiseStudent');

//book issue due list 
Route::get('/due-book-issue-list', 'BookIssue\BookIssueController@dueBookIssuelist')->name('due-book-issue-list');

//activity list 
Route::get('/activity-list', 'BookIssue\BookIssueController@activityList')->name('activity-list');

//fine list 
Route::get('/fine-list', 'BookIssue\BookIssueController@fineList')->name('fine-list');